let w = $(window);
let other_skills = $('#other_skills');
let other_skills_bar = $('#other_skills .progress-bar');
let it_skills = $('#it_skills');
let it_skills_bar = $('#it_skills .progress-bar');


let other_fired = false;
let it_fired = false;


let scroll = () => {
    if (!other_fired){
        if(w.scrollTop() + w.height() > other_skills.offset().top + 50)
        {
            other_skills_bar.each((i, e) => {
                $(e).css('width', $(e).data('width') + '%');
            });

            other_fired = true;
            console.log('OtherFired!');
        }  
    }
    if (!it_fired){
        if(w.scrollTop() + w.height() > it_skills.offset().top + 50)
        {
            it_skills_bar.each((i, e) => {
                $(e).css('width', $(e).data('width') + '%');
            });

            it_fired = true;
            console.log('ITFired!');
        }  
    }

};
scroll();

w.scroll(scroll);